# SYNOPSIS

Create a server listening on the specified socket, then send lines to that
server.
```sh
LMUXDIR=$(mktemp -d)
lmuxd -d "$LMUXDIR" &
export LMUX_SOCKET=$LMUXDIR/socket

lmux -p prefix echo huhu
lmux -p cat <file
exec > >(lmux -p stdout) 2> >(lmux -o stderr)
```

Let the server choose the socket and invoke a command with stdout and stderr
redirected through the server and labeled.  The command can invoke lmux to
send differently-labeled lines.  The server stops accepting new connections
when the command exits, and returns when the last existing connection is
closed.
```sh
lmuxd -o stdout -e stderr sh -c '
  lmux -p prefix echo huhu
  lmux-send -p cat <file
'
```

Let the server choose the socket and invoke a command.  Stdout and stderr of
the command is not redirected, anything output there will bypass the server.
The command can still invoke lmux explicitly to send lines to the server.
```sh
lmuxd sh -c '
  lmux -p prefix echo huhu
  lmux -p cat <file
'
```

# Options

- `lmux -e PREFIX [--] CMD ARGS...` Run `CMD ARGS...`, redirecting it's stderr
  to the lmux server, and prefixing every line by `PREFIX`
- `lmux -o PREFIX [--] CMD ARGS...` Run `CMD ARGS...`, redirecting it's stdout
  to the lmux server, and prefixing every line by `PREFIX`
- `lmux -p PREFIX [--] CMD ARGS...` Run `CMD ARGS...`, redirecting both it's
  stdout and stderr to the lmux server, and prefixing every line by `PREFIX`
- `lmux [--] CMD ARGS...` Behaves as if `-o "CMD: "` was specified
- `lmux [-{o|p} PREFIX]` When invoked without command, behaves as if the
  command was `cat`.  Omitting the redirection specification is equivalent to
  `-o "stdin: "`

- `lmuxd [--] CMD ARGS...` Set up a server socket, then spawn `CMD ARGS...`
  and wait for it to finish.  Stop accepting connections at the server socket
  and clean up the socket, but continue serving existing connections.  Once
  the last connection is closed, exit with the status of `CMD ARGS...`.
- `lmuxd -{e|o|p} PREFIX [--] [CMD ARGS...]` If a redirection is specified,
  run the command through `lmux` with the given redirection options.  The
  command is optional in this case.
- `lmuxd -s SOCKET` Serve at SOCKET.  If SOCKET exists, it is removed and
  recreated (though not atomically).  It is cleaned up before exit.  Go into
  stop mode when the parent pid changes to 1 (this typically happens when the
  invoking process exits and its children are reassigned to init).
- `lmuxd -d DIR` Like `-s DIR/socket`, but also remove DIR upon exit.  Useful
  for creating a temporary directory with `mktemp -d` to put the socket in.
- `lmuxd -stop` Tell a running lmuxd to stop accepting connections.
- `lmuxd -hangup` Tell a running lmuxd to stop accepting connections, and to
  terminate any existing connections immediately.
- `lmuxd -kill` Tell a running lmuxd to terminate as quickly as possible.
  Stop accepting connections, drop existing connections, do not wait for a
  spawned command to finish.

# Environment

- `LMUX_SOCKET` Path of unix domain socket the server is listening on.

# TODO
- Investigate context.Context rather than aborting the readers manually
