package lambda

import (
	"flag"
)

type Value func(string) error

func (self Value) String() string {
	return "(unknown lambda.Value)"
}
func (self Value) Set(value string) error {
	return func(string) error(self)(value)
}

func Var(name, usage string, set interface{}) {
	var setter Value
	switch tset := set.(type) {
	case func(string) error:
		setter = tset
	case func(string):
		setter = func(v string) error {
			tset(v)
			return nil
		}
	default:
		panic("Unsuported setter type")
	}
	flag.Var(setter, name, usage)
}
