package cmdtest

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

var tmpdir *string

func initTmpdir() {
	if tmpdir == nil {
		dir, err := ioutil.TempDir("", "")
		if err != nil {
			panic(err)
		}
		tmpdir = &dir
		err = os.Setenv("GOBIN", dir)
		if err != nil {
			panic(err)
		}
		path := strings.Split(os.Getenv("PATH"), ":")
		path = append([]string{dir}, path...)
		err = os.Setenv("PATH", strings.Join(path, ":"))
		if err != nil {
			panic(err)
		}
	}
}

func Compile() {
	initTmpdir()
	install := exec.Command("go", "install", "./...")
	install.Stdout = os.Stdout
	install.Stderr = os.Stderr
	err := install.Run()
	if err != nil {
		panic(err)
	}
}

func Cleanup() {
	if tmpdir != nil {
		os.RemoveAll(*tmpdir)
	}
}
