package lmuxclient

import (
	"bytes"
	"net"
	"os"
	"os/exec"
	"syscall"
)

func redirect(socketpath string, prefix []byte, tofd int) {
	addr, err := net.ResolveUnixAddr("unix", socketpath)
	if err != nil { panic(err); }
	conn, err := net.DialUnix("unix", nil, addr)
	if err != nil { panic(err); }
	defer conn.Close()

	head := bytes.Join([][]byte{ []byte("cat\n"), prefix, {'\n'} }, nil)
	conn.Write(head)

	rawConn, err := conn.SyscallConn()
	if err != nil { panic(err); }
	err = rawConn.Control(func(fd uintptr){
		err := syscall.Dup2(int(fd), tofd)
		if err != nil { panic(err); }
	})
	if err != nil { panic(err); }
}

func Run(socketpath string, stdoutPrefix, stderrPrefix []byte, cmd []string) {
	// redirect
	if stdoutPrefix != nil { redirect(socketpath, stdoutPrefix, 1); }
	if stderrPrefix != nil { redirect(socketpath, stderrPrefix, 2); }

	// default: run cat
	if len(cmd) == 0 { cmd = []string{ "cat" }; }

	argv0, err := exec.LookPath(cmd[0])
	if err != nil { panic(err); }
	err = syscall.Exec(argv0, cmd, os.Environ())
	panic(err)
}
