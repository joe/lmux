package main

import (
	"flag"
	"io/ioutil"
	"jorrit.de/lmux/internal/flag/lambda"
	"os"
	"os/exec"
	"path/filepath"
	server "jorrit.de/lmux/lmuxserver"
	"syscall"
)

func run() int {
	// Set up socketpath
	var socketpath *string
	lambda.Var("s", "Use this socket", func(v string) {
		socketpath = &v
	})

	// look for redirection flags
	var redirflags []string
	lambda.Var("o", "Redirect stdout and prefix by this", func(v string) {
		redirflags = append(redirflags, "-o", v)
	})
	lambda.Var("e", "Redirect stderr and prefix by this", func(v string) {
		redirflags = append(redirflags, "-e", v)
	})
	lambda.Var("p", "Redirect both stdout and stderr and prefix by this",
		func(v string) {
			redirflags = append(redirflags, "-p", v)
		})

	flag.Parse()

	// Set up command
	args := flag.Args()
	if len(redirflags) > 0 {
		args = append(append([]string{"lmux"}, redirflags...), args...)
	}
	var command *exec.Cmd
	if len(args) > 0 {
		command = exec.Command(args[0], args[1:]...)
		command.Stdin = os.Stdin
		command.Stdout = os.Stdout
		command.Stderr = os.Stderr
	}

	// if there is no socket path, make one up
	if socketpath == nil {
		dir, err := ioutil.TempDir("", "")
		if err != nil {
			panic(err)
		}
		defer os.RemoveAll(dir)

		socketpath = new(string)
		*socketpath = filepath.Join(dir, "socket")
	}

	result := server.Run(*socketpath, command)


	if result == nil { return 0; }
	if ee, ok := result.(*exec.ExitError); ok {
		if ws, ok := ee.Sys().(syscall.WaitStatus); ok {
			if ws.Exited()   { return ws.ExitStatus();        }
			if ws.Signaled() { return int(ws.Signal()) + 128; }
		}
	}
	return 127
}

func main() {
	os.Exit(run());
}
