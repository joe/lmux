package main_test

import (
	"jorrit.de/lmux/internal/cmdtest"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {
	body := func() int {
		defer cmdtest.Cleanup()
		cmdtest.Compile()
		return m.Run()
	}
	os.Exit(body())
}

func TestLmuxdHelp(t *testing.T) {
	output, err := exec.Command("lmuxd", "--help").CombinedOutput()
	t.Log(string(output))
	if strings.Index(string(output), "Usage of lmuxd:") == -1 {
		t.Error("Error: Ouput does not contain expected string")
	}
	t.Log(err)
	if e, ok := err.(*exec.ExitError); !(ok && e.Exited()) {
		t.Error("Error: Program did not terminate normally")
	}
}
