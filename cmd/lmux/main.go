package main

import (
	"bytes"
	client "jorrit.de/lmux/lmuxclient"
	"flag"
	"jorrit.de/lmux/internal/flag/lambda"
	"os"
)

func main() {
	var stdoutPrefix []byte
	lambda.Var("o", "Redirect stdout and prefix by this", func(v string) {
		stdoutPrefix = []byte(v)
	})
	var stderrPrefix []byte
	lambda.Var("e", "Redirect stderr and prefix by this", func(v string) {
		stderrPrefix = []byte(v)
	})
	lambda.Var("p", "Redirect both stdout and stderr and prefix by this",
		func(v string) {
			stdoutPrefix = []byte(v)
			stderrPrefix = []byte(v)
		})
	flag.Parse()

	if stdoutPrefix == nil && stderrPrefix == nil {
		if(flag.NArg() == 0) {
			stdoutPrefix = []byte("stdin: ")
		} else {
			var b bytes.Buffer
			b.WriteString(flag.Args()[0])
			b.WriteString(": ")
			stdoutPrefix = b.Bytes()
		}
	}

	socketpath := os.Getenv("LMUX_SOCKET")
	client.Run(socketpath, stdoutPrefix, stderrPrefix, flag.Args())
}
