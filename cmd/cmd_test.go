package cmd_test

import (
	"context"
	"fmt"
	"jorrit.de/lmux/internal/cmdtest"
	"os"
	"os/exec"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	body := func() int {
		defer cmdtest.Cleanup()
		cmdtest.Compile()
		return m.Run()
	}
	os.Exit(body())
}

func Example_lmux_stdout_cmd() {
	cmd := exec.Command("sh", "-c", `
tmpdir=
trap 'rm -rf "$tmpdir"' 0
tmpdir=$(mktemp -d) || exit 1
LMUX_SOCKET=$tmpdir/socket
export LMUX_SOCKET
lmuxd -s "$LMUX_SOCKET" &
lmuxdpid=$!
while ! [ -S "$LMUX_SOCKET" ]; do sleep 1; done
lmux -o 'stdout: ' echo huhu >/dev/null
exit=$?
kill -9 "$lmuxdpid"
wait
exit "$exit"
`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: stdout: huhu
}


func Example_lmux_stderr_cmd() {
	cmd := exec.Command("sh", "-c", `
tmpdir=
trap 'rm -rf "$tmpdir"' 0
tmpdir=$(mktemp -d) || exit 1
LMUX_SOCKET=$tmpdir/socket
export LMUX_SOCKET
lmuxd -s "$LMUX_SOCKET" &
lmuxdpid=$!
while ! [ -S "$LMUX_SOCKET" ]; do sleep 1; done
lmux -e 'stderr: ' sh -c 'echo huhu >&2' 2>/dev/null
exit=$?
kill -9 "$lmuxdpid"
wait
exit "$exit"
`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: stderr: huhu
}

func Example_lmux_combined_cmd() {
	cmd := exec.Command("sh", "-c", `
tmpdir=
trap 'rm -rf "$tmpdir"' 0
tmpdir=$(mktemp -d) || exit 1
LMUX_SOCKET=$tmpdir/socket
export LMUX_SOCKET
lmuxd -s "$LMUX_SOCKET" &
lmuxdpid=$!
while ! [ -S "$LMUX_SOCKET" ]; do sleep 1; done
lmux -p 'combined: ' sh -c 'echo stdout& echo stderr >&2; wait' >/dev/null 2>&1
exit=$?
kill -9 "$lmuxdpid"
wait
exit "$exit"
`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Unordered output: combined: stdout
	// combined: stderr
}

func Example_lmux_cmd() {
	cmd := exec.Command("sh", "-c", `
tmpdir=
trap 'rm -rf "$tmpdir"' 0
tmpdir=$(mktemp -d) || exit 1
LMUX_SOCKET=$tmpdir/socket
export LMUX_SOCKET
lmuxd -s "$LMUX_SOCKET" &
lmuxdpid=$!
while ! [ -S "$LMUX_SOCKET" ]; do sleep 1; done
lmux echo stdout >&2
exit=$?
kill -9 "$lmuxdpid"
wait
exit "$exit"
`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: echo: stdout
}

func Example_lmux_nocmd() {
	cmd := exec.Command("sh", "-c", `
tmpdir=
trap 'rm -rf "$tmpdir"' 0
tmpdir=$(mktemp -d) || exit 1
LMUX_SOCKET=$tmpdir/socket
export LMUX_SOCKET
lmuxd -s "$LMUX_SOCKET" &
lmuxdpid=$!
while ! [ -S "$LMUX_SOCKET" ]; do sleep 1; done
lmux -o 'stdout: ' <<EOF
huhu
EOF
lmux -p 'combined: ' <<EOF
huhu
EOF
lmux <<EOF
huhu
EOF
exit=$?
kill -9 "$lmuxdpid"
wait
exit "$exit"
`)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: stdout: huhu
	// combined: huhu
	// stdin: huhu
}

func Example_lmuxd_cmd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "lmuxd", "echo", "huhu")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: huhu
}

func Example_lmuxd_lmux_cmd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "lmuxd", "lmux", "echo", "huhu")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: echo: huhu
}

func Example_lmuxd_cmd_exit() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "lmuxd", "sh", "-c", "exit 23")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: Script exited:  exit status 23
}

func Example_lmuxd_cmd_kill() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "lmuxd", "sh", "-c", "kill -9 $$")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: Script exited:  exit status 137
}

func Example_lmuxd_stdout_cmd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "lmuxd", "-o", "stdout: ", "echo", "huhu")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: stdout: huhu
}

func Example_lmuxd_stderr_cmd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx,
		"lmuxd", "-e", "stderr: ", "sh", "-c", "echo huhu >&2")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Output: stderr: huhu
}

func Example_lmuxd_combined_cmd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx,
		"lmuxd", "-p", "combined: ", "sh", "-c",
		"echo stdout; echo stderr >&2")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("Script exited: ", err)
	}
	// Unordered output: combined: stdout
	// combined: stderr
}

