package lmuxserver

import (
	"bufio"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"sync"
)

type Server struct {
	listener net.Listener
	pool     bufferPool
	pipe     chan []byte
	abort    chan struct{}
}

func Make(socketpath string) Server {
	listener, err := net.Listen("unix", socketpath)
	if err != nil { panic(err); }

	return Server{
		listener: listener,
		pool:     makeBufferPool(),
		pipe:     make(chan []byte),
		abort:    make(chan struct{}),
	}
}

func (s *Server) writer(stream io.Writer) {
	for line := range s.pipe {
		_, err := stream.Write(line)
		if err != nil { panic(err); }
		s.pool.put(line)
	}
}

func (s *Server) reader(conn net.Conn) {
	defer conn.Close()

	// set up abort watchdog
	done := make(chan struct{})
	go func() {
		select {
		case <-done: /* nothing */
		case <-s.abort:
			// close the connection so the read will return immediately
			conn.Close()
		}
	}()
	defer close(done)
	
	scanner := bufio.NewScanner(conn)
	scanner.Split(scanFullLines)

	getline := func() ([]byte, bool) {
		if scanner.Scan() {
			line := scanner.Bytes()
			if len(line) > 0 && line[len(line)-1] == '\n' {
				return line, true
			}
		}
		log.Println("lmux: unexpectedly closed connection")
		return []byte{}, false
	}

	cmd, ok := getline()
	if !ok { return; }
	cmd = cmd[:len(cmd)-1];

	switch string(cmd) {
	case "abort":
		close(s.abort)
	case "cat":
		prefix, ok := getline()
		if !ok { return; }

		// make a copy of the prefix string (excluding the newline)
		prefix = append([]byte{}, prefix[:len(prefix)-1]...)

		for scanner.Scan() {
			buf := s.pool.get()
			buf = append(buf, prefix...)
			buf = append(buf, scanner.Bytes()...)
			if buf[len(buf)-1] != '\n' { buf = append(buf, '\n'); }
			s.pipe <- buf;
		}
	case "shutdown":
		s.listener.Close()
	default:
		log.Printf("lmux: bad command %#v\n", string(cmd))
	}

}

func Run(socketpath string, command *exec.Cmd) error {

	s := Make(socketpath)

	// setup writer
	writerDone := make(chan struct {})
	go func(){ s.writer(os.Stdout); close(writerDone); }()
	defer func() { close(s.pipe); <-writerDone; }()

	// setup command
	var result error
	err := os.Setenv("LMUX_SOCKET", socketpath)
	if err != nil { panic(err); }
	commandDone := make(chan struct {})
	defer func() { <-commandDone; }()
	if command != nil {
		go func() {
			defer close(commandDone)
			defer s.listener.Close()
			result = command.Run()
		}()
	} else {
		close(commandDone)
	}

	var readerWait sync.WaitGroup
	// setup readers
	for {
		conn, err := s.listener.Accept()
		if err != nil { break; }
		readerWait.Add(1)
		go func() { s.reader(conn); readerWait.Done(); }()
	}
	readerWait.Wait()

	return result
}
