package lmuxserver

import (
	"bytes"
)

func scanFullLines(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if i := bytes.IndexByte(data, '\n'); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, data[0:i+1], nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF && len(data) > 0 {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}
