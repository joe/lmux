package lmuxserver

type bufferPool chan []byte

func makeBufferPool() bufferPool {
	return make(chan []byte, 20)
}

func (p *bufferPool) get() []byte {
	select {
	case buf := <-*p: return buf
	default:          return []byte{}
	}
}

func (p *bufferPool) put(buf []byte) {
	select {
	case *p<- buf[:0]: /*nothing*/
	default:           /*nothing*/
	}
}
